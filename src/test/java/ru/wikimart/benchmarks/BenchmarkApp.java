package ru.wikimart.benchmarks;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import ru.wikimart.Application;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;

public class BenchmarkApp {
    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(BenchmarkApp.class.getSimpleName())
                .forks(1)
                .warmupIterations(3)
                .measurementIterations(3)
                .mode(Mode.AverageTime)
                .build();
        new Runner(opt).run();
    }
    @Benchmark
    public static void testApp() throws JAXBException, InterruptedException, XMLStreamException, FileNotFoundException {
        Application.main(new String[]{"examples/yandex1.xml", "examples/yandex2.xml"});
    }
}
