package ru.wikimart;

import ru.wikimart.context.Context;
import ru.wikimart.tasks.CompareOffersTask;
import ru.wikimart.tasks.XMLParserTask;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.atomic.AtomicInteger;

public class Application {

    public static final AtomicInteger httpRequestCount = new AtomicInteger();

    public static void main(String[] args) throws XMLStreamException, JAXBException, InterruptedException, FileNotFoundException {
        long startTime = new Date().getTime();
        analyzeArguments(args);
        Context context = new Context(args);
        CompletableFuture.supplyAsync(() -> {
            ForkJoinTask t1 = context.getTasksPool().submit(new XMLParserTask(context, true));
            ForkJoinTask t2 = context.getTasksPool().submit(new XMLParserTask(context, false));
            t1.join();
            t2.join();
            return true;
        }, context.getTasksPool()).whenCompleteAsync((t, e) -> {
            ForkJoinTask t3 = context.getTasksPool().submit(new CompareOffersTask(context));
            t3.join();
        }, context.getTasksPool()).join();
        long endTime = new Date().getTime();
        System.out.println("Elapsed time: " + (endTime - startTime) + " milliseconds");
    }

    private static void analyzeArguments(String[] args) {
        if (args.length < 2) throw new IllegalArgumentException("Wrong number of arguments");
    }
}
