package ru.wikimart.context;

import lombok.Getter;
import ru.wikimart.models.Offer;
import ru.wikimart.models.OfferShort;
import ru.wikimart.models.Param;
import ru.wikimart.services.XMLParserService;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;

@Getter
public class Context {
    private final Map<Long, OfferShort> newOffers = new ConcurrentHashMap<>();
    private final Map <Long, OfferShort> oldOffers = new ConcurrentHashMap<>();
    public final ForkJoinPool tasksPool = new ForkJoinPool(64);
    private JAXBContext jaxbContext = JAXBContext.newInstance(Offer.class, Param.class);
    private XMLStreamReader streamReaderFromNew;
    private XMLStreamReader streamReaderFromOld;
    private XMLParserService xmlParserSevice = new XMLParserService(this);

    public Context(String[] args) throws JAXBException, XMLStreamException, FileNotFoundException {
        initConfig(args);
    }

    private void initConfig(String[] args) throws JAXBException, XMLStreamException, FileNotFoundException {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        this.streamReaderFromOld = inputFactory.createXMLStreamReader(new FileInputStream(args[0]));
        this.streamReaderFromNew = inputFactory.createXMLStreamReader(new FileInputStream(args[1]));
    }
}
