package ru.wikimart.services;

import ru.wikimart.context.Context;
import ru.wikimart.models.Offer;
import ru.wikimart.models.OfferShort;
import ru.wikimart.tasks.HttpPicturesTask;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

public class XMLParserService {
    private Context context;

    public XMLParserService(Context context) {
        this.context = context;
    }

    public void parseOffers(boolean isNewFile) throws XMLStreamException, JAXBException {
        XMLStreamReader streamReader;
        if (isNewFile) {
            streamReader = context.getStreamReaderFromNew();
        } else {
            streamReader = context.getStreamReaderFromOld();
        }
        Unmarshaller unmarshaller = context.getJaxbContext().createUnmarshaller();
        while (streamReader.hasNext()) {
            streamReader.nextTag();
            if (streamReader.getName().toString().toLowerCase().equals("shop")){
                while (streamReader.hasNext()) {
                    streamReader.next();
                    if (streamReader.getEventType() != XMLEvent.START_ELEMENT) continue;
                    if (streamReader.getName().toString().toLowerCase().equals("offers")) {
                        while (streamReader.hasNext()) {
                            streamReader.next();
                            if (streamReader.getEventType() != XMLEvent.START_ELEMENT) continue;
                            if (streamReader.getName().toString().toLowerCase().equals("offer")) {
                                JAXBElement<Offer> offer = unmarshaller.unmarshal(streamReader, Offer.class);
                                OfferShort offerShort = new OfferShort(offer.getValue());
                                if (isNewFile) {
                                    context.getNewOffers().put(offer.getValue().getId(), offerShort);
                                    context.tasksPool.submit(new HttpPicturesTask(offerShort));
                                } else {
                                    context.getOldOffers().put(offer.getValue().getId(), offerShort);
                                }

                            }
                        }
                    }
                }
            }
        }
    }
}
