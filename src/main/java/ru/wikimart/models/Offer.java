package ru.wikimart.models;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.net.URL;
import java.util.List;
import java.util.Objects;

@Setter
@Getter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Offer {
    @XmlAttribute
    private long id;
    @XmlAttribute
    private boolean available;
    @XmlAttribute
    private String type;
    private String url;
    private long price;
    private long oldprice;
    private String currencyId;
    private long categoryId;
    @XmlElement(name = "picture")
    private List<URL> pictures;
    private boolean delivery;
    @XmlElement(name = "local_delivery_cost")
    private long localDeliveryCost;
    private String typePrefix;
    private String vendor;
    private String vendorCode;
    private String model;
    private String description;
    @XmlElement(name = "manufacturer_warranty")
    private boolean manufacturerWarranty;
    @XmlElement(name = "param")
    private List<Param> params;

    @Override
    public int hashCode() {
        return Objects.hash(id, available, type, url, price, oldprice, currencyId, categoryId, pictures, delivery, localDeliveryCost, typePrefix, vendor, vendorCode, model, description, manufacturerWarranty, params);
    }
}
