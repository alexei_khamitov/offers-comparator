package ru.wikimart.models;

import lombok.Data;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

@Data
public class Param {

    private String name;
    private String value;
    private String unit;

    public String getName() {
        return name;
    }
    @XmlAttribute
    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }
    @XmlValue
    public void setValue(String value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }
    @XmlAttribute
    public void setUnit(String unit) {
        this.unit = unit;
    }
}
