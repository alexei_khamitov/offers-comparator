package ru.wikimart.models;

import lombok.Getter;
import lombok.Setter;

import java.net.URL;
import java.util.List;

@Getter
@Setter
public class OfferShort {
    private long id;
    private int hash;
    private boolean actualPictures;
    private boolean checkedPictures;
    private boolean checkedModified;
    private boolean modified;
    private List<URL> pictures;

    public OfferShort(Offer offer) {
        this.id = offer.getId();
        this.hash = offer.hashCode();
        this.pictures = offer.getPictures();
    }
}
