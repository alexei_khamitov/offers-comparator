package ru.wikimart.models;

public enum OfferState {
    MODIFIED("m"),
    REMOVED("r"),
    NEW("n"),
    PICTURE_PROBLEMS("p");

    private String state;

    OfferState(String state) {
        this.state = state;
    }

    public String toString() {
        return this.state;
    }
}
