package ru.wikimart.tasks;

import ru.wikimart.context.Context;
import ru.wikimart.models.Offer;
import ru.wikimart.models.OfferShort;
import ru.wikimart.models.OfferState;

import java.util.Map;
import java.util.concurrent.ForkJoinTask;

public class CompareOffersTask extends ForkJoinTask<Offer> {
    private Context context;

    public CompareOffersTask(Context context) {
        this.context = context;
    }
    @Override
    public Offer getRawResult() {
        return null;
    }

    @Override
    protected void setRawResult(Offer value) {
    }

    @Override
    protected boolean exec() {
        while (true) {
            if (context.getNewOffers().size() > 0) {
                for (Map.Entry<Long, OfferShort> entry : context.getNewOffers().entrySet()) {
                    OfferShort oldOffer = context.getOldOffers().get(entry.getKey());
                    OfferShort newOffer = entry.getValue();
                    compareAndRemoveOffersAndPrintResult(newOffer, oldOffer);
                }
            } else {
                break;
            }
        }
        for (Map.Entry<Long, OfferShort> entry : context.getOldOffers().entrySet()) {
            System.out.println(entry.getValue().getId() + " " + OfferState.REMOVED);
        }
        return true;
    }

    private void removeOffer(OfferShort offer) {
        context.getNewOffers().remove(offer.getId());
        context.getOldOffers().remove(offer.getId());
    }

    private void compareAndRemoveOffersAndPrintResult(OfferShort newOffer, OfferShort oldOffer) {
        if (oldOffer != null) {
            if (newOffer.isCheckedPictures()) {
                int countOfStatuses = 0;
                StringBuilder offerTotalState = new StringBuilder();
                offerTotalState.append(newOffer.getId()).append(" ");
                if (!newOffer.isActualPictures()) {
                    offerTotalState.append(OfferState.PICTURE_PROBLEMS);
                    countOfStatuses++;
                }
                if (newOffer.isCheckedModified() && newOffer.isModified()
                        || !(newOffer.getHash() == oldOffer.getHash())) {
                    offerTotalState.append(OfferState.MODIFIED);
                    countOfStatuses++;
                }
                if (countOfStatuses > 0) System.out.println(offerTotalState);
                removeOffer(newOffer);
            } else {
                if (newOffer.getHash() == oldOffer.getHash()) {
                    newOffer.setModified(false);
                } else {
                    newOffer.setModified(true);
                }
                newOffer.setCheckedModified(true);
            }
        } else {
            if (newOffer.isCheckedPictures()) {
                StringBuilder offerTotalState = new StringBuilder();
                offerTotalState.append(newOffer.getId()).append(" ");
                if (!newOffer.isActualPictures()) {
                    offerTotalState.append(OfferState.PICTURE_PROBLEMS);
                }
                offerTotalState.append(OfferState.NEW);
                System.out.println(offerTotalState.toString());
                removeOffer(newOffer);
            }

        }
    }

}
