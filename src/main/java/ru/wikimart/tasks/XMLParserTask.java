package ru.wikimart.tasks;

import ru.wikimart.context.Context;
import ru.wikimart.models.Offer;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.util.concurrent.ForkJoinTask;

public class XMLParserTask extends ForkJoinTask<Offer> {
    private boolean isNewOffers;
    private Context context;

    public XMLParserTask(Context context, boolean isNewOffers) {
        this.isNewOffers = isNewOffers;
        this.context = context;
    }
    @Override
    public Offer getRawResult() {
        return null;
    }

    @Override
    protected void setRawResult(Offer value) {

    }

    @Override
    protected boolean exec() {
        try {
            context.getXmlParserSevice().parseOffers(this.isNewOffers);
        } catch (XMLStreamException | JAXBException e) {
            e.printStackTrace();
        }
        return true;
    }
}
