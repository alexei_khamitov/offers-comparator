package ru.wikimart.tasks;

import ru.wikimart.models.Offer;
import ru.wikimart.models.OfferShort;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ForkJoinTask;

public class HttpPicturesTask extends ForkJoinTask<Offer> {
    private OfferShort offer;

    public HttpPicturesTask(OfferShort offer) {
        this.offer = offer;
    }

    @Override
    public Offer getRawResult() {
        return null;
    }

    @Override
    protected void setRawResult(Offer value) {

    }

    @Override
    protected boolean exec() {
        if (this.offer.getPictures() == null) {
            this.offer.setCheckedPictures(true);
            this.offer.setActualPictures(false);
            return true;
        }
        for (URL urlOfPicture : this.offer.getPictures()) {
            try {
                HttpURLConnection connection = (HttpURLConnection) urlOfPicture.openConnection();
                connection.setRequestMethod("HEAD");
                connection.connect();
                connection.getInputStream().close();
            } catch (IOException e) {

                this.offer.setCheckedPictures(true);
                this.offer.setActualPictures(false);
                this.offer.getPictures().clear();
                return true;
            }
        }
        this.offer.setCheckedPictures(true);
        this.offer.setActualPictures(true);
        this.offer.getPictures().clear();
        return true;
    }

    @Override
    public void complete(Offer value) {
        super.complete(value);
    }
}
